﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PacmanMove : MonoBehaviour {
	public float speed = 0.4f;
	Vector2 dest = Vector2.zero;
	Vector3 rightEnd ;
	Vector3 leftEnd ;
	void Start() {
		float x1 = 28f,x2 = 1f,y = 17f,z = 0f;
		rightEnd.Set(x1,y,z);
		leftEnd.Set(x2,y,z);
		dest = transform.position;
	}
	
	void FixedUpdate() {

		if (transform.position.y == rightEnd.y && transform.position.x > rightEnd.x ) {
			//GetComponent<Rigidbody2D>().GetVector() = leftEnd;
			transform.position = leftEnd;
			dest = transform.position;
			Debug.Log (dest);
		}
		else
		if(transform.position.y == leftEnd.y && transform.position.x < leftEnd.x ) {
			//GetComponent<Rigidbody2D>().GetVector() = leftEnd;
			transform.position = rightEnd;
			dest = transform.position;
			Debug.Log (dest);
		}



		// Move closer to Destination
		Vector2 p = Vector2.MoveTowards(transform.position, dest, speed);
		GetComponent<Rigidbody2D>().MovePosition(p);
		
		// Check for Input if not moving
		if ((Vector2)transform.position == dest) {
			if (Input.GetKey(KeyCode.UpArrow) && valid(Vector2.up))
				dest = (Vector2)transform.position + Vector2.up;
			if (Input.GetKey(KeyCode.RightArrow) && valid(Vector2.right))
				dest = (Vector2)transform.position + Vector2.right;
			if (Input.GetKey(KeyCode.DownArrow) && valid(-Vector2.up))
				dest = (Vector2)transform.position - Vector2.up;
			if (Input.GetKey(KeyCode.LeftArrow) && valid(-Vector2.right))
				dest = (Vector2)transform.position - Vector2.right;
		}

		// AI based dicision
	/*	if ((Vector2)transform.position == dest) {
			dest = chooseAction(dest);

		}*/
		
		// Animation Parameters
		Vector2 dir = dest - (Vector2)transform.position;
		GetComponent<Animator>().SetFloat("DirX", dir.x);
		GetComponent<Animator>().SetFloat("DirY", dir.y);

		// store the position of pacman
	}
	
	bool valid(Vector2 dir) {
		// Cast Line from 'next to Pac-Man' to 'Pac-Man'
		Vector2 pos = transform.position;
		RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);
		return (hit.collider == GetComponent<Collider2D>());
	}

	/*Vector2 chooseAction(Vector2 cur)
	{
		int n;
		bool upAntE, rightAntE, downAntE, leftAntE, upAntC, rightAntC, downAntC, leftAntC;

	}*/

}
﻿using UnityEngine;
using System.Collections;

//[assembly: InternalsVisibleToAttribute("Pacdot")]
//namespace Utilities.IntUtilities

public class GhostMove : MonoBehaviour {
	public Transform[] waypoints;
	int cur = 0;
	static float timer = 0.0f ;
	public static int isEdible=0;    
	int eaten = 0;
	public static float speed = 0.1f;    // security issue here , as variabble declayered as public


	private IEnumerator remainEdible(float seconds)
	{

		yield return new WaitForSeconds (seconds);
		GhostMove.speed = 0.1f;
		GhostMove.isEdible = 0;
	}




void FixedUpdate ()
	{


		if (isEdible == 1) 
		{
			StartCoroutine(remainEdible(10.0f));
		}
		if (transform.position != waypoints [cur].position) 
		{
			Vector2 p = Vector2.MoveTowards (transform.position,
		                                waypoints [cur].position,
		                                speed);
			GetComponent<Rigidbody2D> ().MovePosition (p);
		}
	// Waypoint reached, select next one
	    else
			cur = (cur + 1) % waypoints.Length;
	
		// Animation
		Vector2 dir = waypoints [cur].position - transform.position;
		GetComponent<Animator> ().SetFloat ("DirX", dir.x);
		GetComponent<Animator> ().SetFloat ("DirY", dir.y);
	}


void OnTriggerEnter2D(Collider2D co)
	{
	if (co.name == "pacman") 
		{
			 if (eaten==1) {}
			 else 
			      {

					    if (isEdible == 0 )
				            {
						         Destroy (co.gameObject);
				            }
					    else 
				            {
						         //gameObject.hideFlags = HideFlags.HideAndDontSave;
					             gameObject.SetActiveRecursively(false);  // decativating the ghost
						         Pacdot.score = Pacdot.score + 100;
					             eaten = 1;
						         Invoke ("restartGhost",4);
				             }
			     }
		}
    }

	public void restartGhost()
	{

		transform.position = waypoints [0].position ;
		cur = 0;
		gameObject.SetActiveRecursively(true);
		eaten = 0;
	}
	
	
void OnGUI () 
	{
		if (GameObject.Find ("pacman") == null) 
		{
			GUI.Label (new Rect (650, 300, 100, 20), "Game Over");
			if(GUI.Button(new Rect (600,340,200,20),"Play again"))
			      {
				     // restart the game
			         Application.LoadLevel(Application.loadedLevelName);
		          }
		
	    }

    }
}
